// lib -> registering modules
// entrypoint -> entrypoint to the program
// instruction -> program API, (de)serializing instruction data
// processor -> program logic
// state -> program objects, (de)serializing state
// error -> program-specific errors

pub mod entrypoint;
pub mod instruction;
pub mod processor;
pub mod state;
pub mod error;